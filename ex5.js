/**
 * Input:
 * n = 75
 *
 * Todo:
 * units = n % 10
 * tens = Math.floor(n / 10)
 * sum = units + tens
 *
 * Output:
 * sum = 12
 */

var n = 75;
var units, tens, sum;

units = n % 10;
tens = Math.floor(n / 10);
sum = units + tens;

console.log("sum: ", sum);
