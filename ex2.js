/**
 * Input:
 * n1 = 2.4
 * n2 = 4.8
 * n3 = 1.1
 * n4 = 5.5
 * n5 = 9.7
 *
 * Todo:
 * average = (n1 + n2 + n3 + n4 + n5) / 5
 *
 * Output:
 * average = 4.7
 */

var n1 = 2.4;
var n2 = 4.8;
var n3 = 1.1;
var n4 = 5.5;
var n5 = 9.7;
var average;

average = (n1 + n2 + n3 + n4 + n5) / 5;

console.log("average: ", average);
