/**
 * Input:
 * USD = 70
 *
 * Todo:
 * VND = USD * 23500
 *
 * Output:
 * VND = 1645000
 */

var USD = 70;
var VND;

VND = USD * 23500;

console.log("VND: ", VND);
