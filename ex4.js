/**
 * Input:
 * long = 15
 * width = 7
 *
 * Todo:
 * area = long * width
 * perimeter = (long + width) * 2
 *
 * Output:
 * area = 105
 * perimeter = 44
 */

var long = 15;
var width = 7;
var area, perimeter;

area = long * width;
perimeter = (long + width) * 2;

console.log("area: ", area);
console.log("perimeter: ", perimeter);
