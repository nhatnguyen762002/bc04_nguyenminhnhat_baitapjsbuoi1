/**
 * Input:
 * salary1Day = 100000
 * workDay = 77
 *
 * Todo:
 * totalSalary = salary1Day * workDay
 *
 * Output:
 * totalSalary = 7700000
 */

var salary1Day = 100000;
var workDay = 77;
var totalSalary;

totalSalary = salary1Day * workDay;

console.log("totalSalary: ", totalSalary);
